tailwind.config = {
  theme: {
    extend: {
      colors: {
        gold: "#9E7F32",
        white: "#FFFFFF",
        black: "#000",
      },
    },
  },
};
