//==============================================
//                Sidebar
//==============================================
document.addEventListener("DOMContentLoaded", function () {
  var addButton = document.getElementById("toggle-sidebar");
  var body = document.body;
  if (addButton) {
    addButton.addEventListener("click", function () {
      body.classList.add("show-sidebar");
    });
  }
});

document.addEventListener("DOMContentLoaded", function () {
  const accordionButtons = document.querySelectorAll(".close-sidebar ");
  const body = document.querySelector("body");

  accordionButtons.forEach((button) => {
    button.addEventListener("click", function () {
      // Toggle a class on the body tag
      body.classList.remove("show-sidebar");
    });
  });
});

//==========================================
//              Tabs JS
//==========================================
document.addEventListener("DOMContentLoaded", function () {
  const tabButtons = document.querySelectorAll(".tab-btn");
  const tabContents = document.querySelectorAll(".tab-content");

  tabButtons.forEach((button) => {
    button.addEventListener("click", () => {
      const tabId = button.dataset.tab;
      activateTab(tabId);
    });
  });

  function activateTab(tabId) {
    tabContents.forEach((content) => {
      if (content.id === tabId) {
        content.style.display = "block";
      } else {
        content.style.display = "none";
      }
    });

    tabButtons.forEach((button) => {
      if (button.dataset.tab === tabId) {
        button.classList.add("active");
      } else {
        button.classList.remove("active");
      }
    });
  }
});

// Tab custom

document.addEventListener("DOMContentLoaded", function () {
  const tabs = document.querySelectorAll(".tab-btn");
  const tabContents = document.querySelectorAll(".tab-content-panel");

  tabs.forEach((tab) => {
    tab.addEventListener("click", function () {
      const tabId = this.getAttribute("data-tab");
      activateTab(tabId);
    });
  });

  function activateTab(tabId) {
    tabContents.forEach((content) => {
      if (content.id === tabId) {
        content.classList.add("active");
      } else {
        content.classList.remove("active");
      }
    });

    tabs.forEach((tab) => {
      if (tab.getAttribute("data-tab") === tabId) {
        tab.classList.add("active");
      } else {
        tab.classList.remove("active");
      }
    });
  }
});
//==========================================
//             accordion
//==========================================
document.addEventListener("DOMContentLoaded", () => {
  // Simulate an API request or any async operation
  setTimeout(() => {
    hideLoader();
    showContent();
  }, 2000); // Replace with your actual data loading logic and time

  function hideLoader() {
    const loader = document.getElementById("loader");
    if (loader !== null) {
      loader.style.display = "none";
    }
  }

  function showContent() {
    const content = document.getElementById("content-loading");
    if (loader !== null) {
      content.style.display = "block";
    }
  }
});
//==============================================
//                Modal
//==============================================
document.addEventListener("DOMContentLoaded", function () {
  // Get all modals
  var modals = document.querySelectorAll(".modal");

  // Get all buttons that open a modal
  var modalButtons = document.querySelectorAll(".open-modal");

  // Close modal function
  function closeModal(modal) {
    modal.style.display = "none";
  }

  // Add click event listeners to all modal buttons
  modalButtons.forEach(function (button) {
    button.addEventListener("click", function () {
      // Get the modal ID from the data-id attribute
      var modalId = button.getAttribute("data-id");
      var modal = document.getElementById(modalId);

      // Close all modals
      modals.forEach(function (modal) {
        closeModal(modal);
      });

      // Display the target modal
      modal.style.display = "block";
    });
  });

  // Add click event listener to close buttons
  var closeButtons = document.querySelectorAll(".close");
  closeButtons.forEach(function (button) {
    button.addEventListener("click", function () {
      var modal = button.closest(".modal");
      closeModal(modal);
    });
  });

  // Close modal when clicking outside
  window.addEventListener("click", function (event) {
    modals.forEach(function (modal) {
      if (event.target == modal) {
        closeModal(modal);
      }
    });
  });
});
//==========================================
//             accordion
//==========================================
document.addEventListener("DOMContentLoaded", function () {
  const accordionItems = document.querySelectorAll(".accordion-card-custom");

  accordionItems.forEach((item) => {
    const header = item.querySelector(".accordion-header-custom");

    header.addEventListener("click", () => {
      if (!item.classList.contains("active")) {
        closeAllAccordions();
        item.classList.add("active");
      } else {
        item.classList.remove("active");
      }
    });
  });

  function closeAllAccordions() {
    accordionItems.forEach((item) => {
      item.classList.remove("active");
    });
  }
});

document.addEventListener("DOMContentLoaded", function () {
  const accordionCards = document.querySelectorAll(".accordion-card");

  accordionCards.forEach((card) => {
    const btnShowAccordion = card.querySelector(".btn-show-accordion");
    const accordionBody = card.querySelector(".accordion-body");

    btnShowAccordion.addEventListener("click", function () {
      if (
        accordionBody.style.display === "none" ||
        accordionBody.style.display === ""
      ) {
        // Close all other accordion cards
        closeAllAccordionCards();

        // Open this accordion card
        accordionBody.style.display = "block";
        btnShowAccordion.classList.add("active");
        accordionCards.classList.add("active-accordion");
      } else {
        accordionBody.style.display = "none";
        btnShowAccordion.classList.remove("active");
        accordionCards.classList.remove("active-accordion");
      }
    });
  });

  function closeAllAccordionCards() {
    accordionCards.forEach((card) => {
      const accordionBody = card.querySelector(".accordion-body");
      const btnShowAccordion = card.querySelector(".btn-show-accordion");

      accordionBody.style.display = "none";
      btnShowAccordion.classList.remove("active");
    });
  }
});

document.addEventListener("DOMContentLoaded", function () {
  const accordionCards = document.querySelectorAll(".sub-accordion-card");

  accordionCards.forEach((card) => {
    const btnShowAccordion = card.querySelector(".sub-accordion-card-header");
    const accordionBody = card.querySelector(".sub-accordion-card-body");

    btnShowAccordion.addEventListener("click", function () {
      if (
        accordionBody.style.display === "none" ||
        accordionBody.style.display === ""
      ) {
        // Close all other accordion cards
        closeAllAccordionCards();

        // Open this accordion card
        accordionBody.style.display = "block";
        btnShowAccordion.classList.add("active");
        accordionCards.classList.add("active-accordion");
      } else {
        accordionBody.style.display = "none";
        btnShowAccordion.classList.remove("active");
        accordionCards.classList.remove("active-accordion");
      }
    });
  });
  function closeAllAccordionCards() {
    accordionCards.forEach((card) => {
      const accordionBody = card.querySelector(".sub-accordion-card-body");
      const btnShowAccordion = card.querySelector(".sub-accordion-card-header");

      accordionBody.style.display = "none";
      btnShowAccordion.classList.remove("active");
    });
  }
});

// Custom Accordion

document.addEventListener("DOMContentLoaded", function () {
  const accordionHeaders = document.querySelectorAll(".accordion-card-store");

  accordionHeaders.forEach(function (header) {
    header.addEventListener("click", function () {
      const accordionBody = this.nextElementSibling;
      const isActive = this.classList.contains("active");

      // Remove active class from all headers
      accordionHeaders.forEach(function (header) {
        header.classList.remove("active");
      });

      // Hide all accordion bodies
      const allAccordionBodies = document.querySelectorAll(
        ".accordion-body-store"
      );
      allAccordionBodies.forEach(function (body) {
        body.style.display = "hide-accordion-body";
      });

      // Toggle active class on clicked header
      if (!isActive) {
        this.classList.add("active");
      }

      // Toggle display of accordion body
      if (!isActive) {
        accordionBody.style.display = "show-accordion-body-store";
      }
    });
  });
});

//  Accordion Default

document.addEventListener("DOMContentLoaded", function () {
  const accordionHeaders = document.querySelectorAll(".accordion-card-default");

  accordionHeaders.forEach(function (header) {
    header.addEventListener("click", function () {
      const accordionBody = this.nextElementSibling;
      const isActive = this.classList.contains("active");

      // Remove active class from all headers
      accordionHeaders.forEach(function (header) {
        header.classList.remove("active");
      });

      // Hide all accordion bodies
      const allAccordionBodies = document.querySelectorAll(
        ".accordion-body-default"
      );
      allAccordionBodies.forEach(function (body) {
        body.style.display = "hide-body";
      });

      // Toggle active class on clicked header
      if (!isActive) {
        this.classList.add("active");
      }

      // Toggle display of accordion body
      if (!isActive) {
        accordionBody.style.display = "show-body";
      }
    });
  });
});

//==============================================
//                Select custom
//==============================================
// Get all custom select elements
var customSelects = document.querySelectorAll(".custom-select");

// Add click event listener to each custom select
customSelects.forEach(function (select) {
  var selectSelected = select.querySelector(".select-selected");
  var selectItems = select.querySelector(".select-items");

  // Toggle active class on click
  selectSelected.addEventListener("click", function () {
    select.classList.toggle("active");
    if (select.classList.contains("active")) {
      selectItems.style.display = "block";
    } else {
      selectItems.style.display = "none";
    }
  });

  // Handle selection of an option
  selectItems.querySelectorAll("div").forEach(function (option) {
    option.addEventListener("click", function () {
      var selectedOption = option.innerHTML;
      selectSelected.innerHTML = selectedOption;
      select.classList.remove("active");
      selectItems.style.display = "none";
    });
  });
});
//==============================================
//             Slider Custom
//==============================================
document.addEventListener("DOMContentLoaded", function () {
  const slider = document.querySelector(".slider-custom");
  const slides = document.querySelectorAll(".slide-item");

  slides.forEach((slide, index) => {
    slide.addEventListener("click", function () {
      setActiveSlide(index);
    });
  });

  function setActiveSlide(index) {
    slides.forEach((slide, i) => {
      slide.classList.remove("active");
      if (i === index) {
        slide.classList.add("active");
        scrollToCenter(slide);
      }
    });
  }
  // Disable gesture scroll
  if (slider) {
    slider.addEventListener("touchmove", function (event) {
      event.preventDefault();
    });
  }
  // Add active class to the first slide item initially
  slides[0].classList.add("active");
  function scrollToCenter(slide) {
    const sliderRect = slider.getBoundingClientRect();
    const slideRect = slide.getBoundingClientRect();
    const scrollAmount =
      slideRect.left -
      sliderRect.left -
      (sliderRect.width - slideRect.width) / 2;
    slider.scrollTo({
      left: slider.scrollLeft + scrollAmount,
      behavior: "smooth",
    });
  }
  // Add padding-right to the last slide
  slides[slides.length - 1].style.paddingRight = "10px"; // Adjust padding as needed
});

//==========================================
//             Btn active
//==========================================
document.addEventListener("DOMContentLoaded", function () {
  var parent = document.querySelector(".invoice");
  var button = document.querySelectorAll(".add-to-pay");
  button.forEach(function (button) {
    button.addEventListener("click", function () {
      // Toggle active class
      parent.classList.toggle("active");
    });
  });
});

document.addEventListener("DOMContentLoaded", function () {
  var parent = document.querySelector(".add-to-payment");
  var button = document.querySelectorAll(".btn-detail-item");
  button.forEach(function (button) {
    button.addEventListener("click", function () {
      // Toggle active class
      parent.classList.toggle("active");
    });
  });
});
document.addEventListener("DOMContentLoaded", function () {
  var parent = document.querySelector(".invoice");
  var button = document.querySelectorAll(".btn-all-togle");
  button.forEach(function (button) {
    button.addEventListener("click", function () {
      // Toggle active class
      parent.classList.toggle("paid-unpaid");
    });
  });
});

//==========================================
//            Copy text
//==========================================
document.addEventListener("DOMContentLoaded", function () {
  const copyButton = document.getElementById("copyButton");
  const inputField = document.getElementById("copyText");

  if (copyButton) {
    copyButton.addEventListener("click", function () {
      // Select the text inside the input field
      inputField.select();
      inputField.setSelectionRange(0, 99999); // For mobile devices

      // Copy the selected text to the clipboard
      document.execCommand("copy");
      // Deselect the text
      window.getSelection().removeAllRanges();
      // Provide some visual feedback
      copyButton.innerText = "Text Copied!";
    });
  }
});

//==========================================
//              Show Hide panel
//==========================================
document.addEventListener("DOMContentLoaded", function () {
  const radioButtons = document.querySelectorAll(".item-bank input");
  radioButtons.forEach((button) => {
    button.addEventListener("click", function () {
      radioButtons.forEach((btn) => {
        if (btn !== button) {
          btn.parentNode.classList.remove("active");
        }
      });
      this.parentNode.classList.add("active");
    });
  });
});

//==============================================
//            Show Hide Passsword
//==============================================
// Function to toggle password visibility
function togglePasswordVisibility(targetId) {
  const passwordInput = document.getElementById(targetId);
  // Check if the password input exists
  if (passwordInput) {
    if (passwordInput.type === "password") {
      passwordInput.type = "text";
    } else {
      passwordInput.type = "password";
    }
  }
}
// Attach event listener to buttons with data-id attribute
document.addEventListener("DOMContentLoaded", function () {
  const toggleButtons = document.querySelectorAll("[data-id-password]");
  toggleButtons.forEach(function (button) {
    button.addEventListener("click", function () {
      const targetId = this.getAttribute("data-id-password");
      togglePasswordVisibility(targetId);
    });
  });
});
//==============================================
//           Slider Popup Image
//==============================================
document.addEventListener("DOMContentLoaded", function () {
  const popup = document.getElementById("popup");
  const popupButton = document.getElementById("popupButton");
  const popupImage = document.getElementById("popupImage");
  const closeButton = document.getElementById("closeButton");

  const sliderImages = document.querySelectorAll(".slider-image-item img");

  sliderImages.forEach((img) => {
    img.addEventListener("click", function () {
      const src = this.getAttribute("src");
      popupImage.setAttribute("src", src);
      popup.classList.remove("hidden");
      // Remove 'active' class from all items
      document.querySelectorAll(".slider-image-item").forEach((item) => {
        item.classList.remove("active");
      });
      // Add 'active' class to the clicked item
      this.parentElement.classList.add("active");
    });
  });

  if (popupButton) {
    popupButton.addEventListener("click", function () {
      const activeImage = document.querySelector(
        ".slider-image-item.active img"
      );
      if (activeImage) {
        const src = activeImage.getAttribute("src");
        popupImage.setAttribute("src", src);
        popup.classList.remove("hidden");
      }
    });
  }

  if (closeButton) {
    closeButton.addEventListener("click", function () {
      popup.classList.add("hidden");
    });
  }

  // Close popup when clicking outside the popup
  window.addEventListener("click", function (event) {
    if (event.target === popup) {
      popup.classList.add("hidden");
    }
  });
});
//==============================================
//            Date pickup
//==============================================
document.addEventListener("DOMContentLoaded", function () {
  const months = [
    "January",
    "February",
    "March",
    "April",
    "May",
    "June",
    "July",
    "August",
    "September",
    "October",
    "November",
    "December",
  ];

  const calendarBody = document.getElementById("calendarBody");
  const currentMonthElement = document.getElementById("currentMonth");
  const dropdownCalendar = document.getElementById("dropdown-calender");

  let currentDate = new Date();
  let currentYear = currentDate.getFullYear();
  let currentMonth = currentDate.getMonth();

  // Initial rendering of calendar
  renderCalendar(currentYear, currentMonth);

  // Function to render the calendar
  function renderCalendar(year, month) {
    currentMonthElement.textContent = months[month] + " " + year;
    calendarBody.innerHTML = "";

    const firstDayOfMonth = new Date(year, month, 1).getDay();
    const daysInMonth = new Date(year, month + 1, 0).getDate();

    let date = 1;

    for (let i = 0; i < 6; i++) {
      const row = document.createElement("tr");

      for (let j = 0; j < 7; j++) {
        if (i === 0 && j < firstDayOfMonth) {
          const cell = document.createElement("td");
          row.appendChild(cell);
        } else if (date > daysInMonth) {
          break;
        } else {
          const cell = document.createElement("td");
          cell.textContent = date;

          // Add click event to show dropdown calendar
          cell.addEventListener("click", () => {
            dropdownCalendar.classList.remove("hidden-dropdown-calender");
          });

          row.appendChild(cell);
          date++;
        }
      }
      calendarBody.appendChild(row);
    }
  }

  // Event listener for previous month button
  document.getElementById("prevBtnCalender").addEventListener("click", () => {
    currentMonth--;
    if (currentMonth < 0) {
      currentMonth = 11;
      currentYear--;
    }
    renderCalendar(currentYear, currentMonth);
  });

  // Event listener for next month button
  document.getElementById("nextBtnCalender").addEventListener("click", () => {
    currentMonth++;
    if (currentMonth > 11) {
      currentMonth = 0;
      currentYear++;
    }
    renderCalendar(currentYear, currentMonth);
  });

  // Event listener to hide dropdown calendar
  document.querySelector(".close").addEventListener("click", () => {
    dropdownCalendar.classList.add("hidden-dropdown-calender");
  });
});

//==============================================
//             Upload Image
//==============================================

document.getElementById("upload").addEventListener("change", function () {
  var preview = document.getElementById("preview");
  var file = this.files[0];
  var reader = new FileReader();

  reader.onloadend = function () {
    var img = document.createElement("img");
    img.src = reader.result;
    img.style.maxWidth = "100%";
    preview.innerHTML = "";
    preview.appendChild(img);
  };

  if (file) {
    reader.readAsDataURL(file);
  } else {
    preview.innerHTML = "No image selected";
  }
});
